package com.arun.fifadreamdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@RestController
public class FifaDreamController {

    @Autowired
    RestTemplate restTemplate;

    @Value("${service.url}")
    private String baseUrl;

    @Value("${x-api-key}")
    private String apiKey;

    @Value("${Authorization}")
    private String authorization;

    @RequestMapping(value = "/player_name", method = RequestMethod.GET)
    public String get_player_info(HttpServletRequest request) {
        String uri = request.getRequestURI();
        HttpHeaders headers = new HttpHeaders();
        headers.set("x-api-key", apiKey);
        headers.set("Authorization", authorization);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        return restTemplate.exchange(
                baseUrl + uri, HttpMethod.GET, entity, String.class).getBody();
    }

    @RequestMapping(value = "/club_name", method = RequestMethod.GET)
    public String get_club_player_list(HttpServletRequest request) {
        String uri = request.getRequestURI();
        HttpHeaders headers = new HttpHeaders();
        headers.set("x-api-key", apiKey);
        headers.set("Authorization", authorization);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        return restTemplate.exchange(
                baseUrl + uri, HttpMethod.GET, entity, String.class).getBody();
    }

    @RequestMapping(value = "/club_name/", method = RequestMethod.GET)
    public String strengthen_team(HttpServletRequest request) {
        String uri = request.getRequestURI();
        HttpHeaders headers = new HttpHeaders();
        headers.set("x-api-key", apiKey);
        headers.set("Authorization", authorization);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        return restTemplate.exchange(
                baseUrl + uri, HttpMethod.GET, entity, String.class).getBody();
    }
}
