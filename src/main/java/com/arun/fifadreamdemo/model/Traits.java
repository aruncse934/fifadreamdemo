package com.arun.fifadreamdemo.model;

public class Traits {

    private String Preferred_Positioning;
    private String Aggression;
    private String Agility;
    private String Balance;
    private String Ball_control;
    private String Composure;

    public Traits(String preferred_Positioning, String aggression, String agility, String balance, String ball_control, String composure) {
        Preferred_Positioning = preferred_Positioning;
        Aggression = aggression;
        Agility = agility;
        Balance = balance;
        Ball_control = ball_control;
        Composure = composure;
    }

    public String getPreferred_Positioning() {
        return Preferred_Positioning;
    }

    public void setPreferred_Positioning(String preferred_Positioning) {
        Preferred_Positioning = preferred_Positioning;
    }

    public String getAggression() {
        return Aggression;
    }

    public void setAggression(String aggression) {
        Aggression = aggression;
    }

    public String getAgility() {
        return Agility;
    }

    public void setAgility(String agility) {
        Agility = agility;
    }

    public String getBalance() {
        return Balance;
    }

    public void setBalance(String balance) {
        Balance = balance;
    }

    public String getBall_control() {
        return Ball_control;
    }

    public void setBall_control(String ball_control) {
        Ball_control = ball_control;
    }

    public String getComposure() {
        return Composure;
    }

    public void setComposure(String composure) {
        Composure = composure;
    }

}
