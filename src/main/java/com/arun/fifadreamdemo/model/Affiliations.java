package com.arun.fifadreamdemo.model;

public class Affiliations {

    private String Nationality;
    private String Flag;
    private String Club;
    private  String Club_Logo;

    public Affiliations(String nationality, String flag, String club, String club_Logo) {
        Nationality = nationality;
        Flag = flag;
        Club = club;
        Club_Logo = club_Logo;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public String getClub() {
        return Club;
    }

    public void setClub(String club) {
        Club = club;
    }

    public String getClub_Logo() {
        return Club_Logo;
    }

    public void setClub_Logo(String club_Logo) {
        Club_Logo = club_Logo;
    }

}
