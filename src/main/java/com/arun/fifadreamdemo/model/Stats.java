package com.arun.fifadreamdemo.model;

public class Stats {

    private  String Shot_power;
    private String Sliding_tackle;
    private String Sprint_speed;
    private String Stamina;
    private String Standing_tackle;
    private String Strength;
    private String Vision;
    private String Heading_accuracy;
    private String Interceptions;
    private String Jumping;
    private String Long_passing;
    private String Long_shots;
    private String Marking;
    private String Penalties;
    private String Free_kick_accuracy;

    public Stats(String shot_power, String sliding_tackle, String sprint_speed, String stamina, String standing_tackle, String strength, String vision, String heading_accuracy, String interceptions, String jumping, String long_passing, String long_shots, String marking, String penalties, String free_kick_accuracy) {
        Shot_power = shot_power;
        Sliding_tackle = sliding_tackle;
        Sprint_speed = sprint_speed;
        Stamina = stamina;
        Standing_tackle = standing_tackle;
        Strength = strength;
        Vision = vision;
        Heading_accuracy = heading_accuracy;
        Interceptions = interceptions;
        Jumping = jumping;
        Long_passing = long_passing;
        Long_shots = long_shots;
        Marking = marking;
        Penalties = penalties;
        Free_kick_accuracy = free_kick_accuracy;
    }

    public String getShot_power() {
        return Shot_power;
    }

    public void setShot_power(String shot_power) {
        Shot_power = shot_power;
    }

    public String getSliding_tackle() {
        return Sliding_tackle;
    }

    public void setSliding_tackle(String sliding_tackle) {
        Sliding_tackle = sliding_tackle;
    }

    public String getSprint_speed() {
        return Sprint_speed;
    }

    public void setSprint_speed(String sprint_speed) {
        Sprint_speed = sprint_speed;
    }

    public String getStamina() {
        return Stamina;
    }

    public void setStamina(String stamina) {
        Stamina = stamina;
    }

    public String getStanding_tackle() {
        return Standing_tackle;
    }

    public void setStanding_tackle(String standing_tackle) {
        Standing_tackle = standing_tackle;
    }

    public String getStrength() {
        return Strength;
    }

    public void setStrength(String strength) {
        Strength = strength;
    }

    public String getVision() {
        return Vision;
    }

    public void setVision(String vision) {
        Vision = vision;
    }

    public String getHeading_accuracy() {
        return Heading_accuracy;
    }

    public void setHeading_accuracy(String heading_accuracy) {
        Heading_accuracy = heading_accuracy;
    }

    public String getInterceptions() {
        return Interceptions;
    }

    public void setInterceptions(String interceptions) {
        Interceptions = interceptions;
    }

    public String getJumping() {
        return Jumping;
    }

    public void setJumping(String jumping) {
        Jumping = jumping;
    }

    public String getLong_passing() {
        return Long_passing;
    }

    public void setLong_passing(String long_passing) {
        Long_passing = long_passing;
    }

    public String getLong_shots() {
        return Long_shots;
    }

    public void setLong_shots(String long_shots) {
        Long_shots = long_shots;
    }

    public String getMarking() {
        return Marking;
    }

    public void setMarking(String marking) {
        Marking = marking;
    }

    public String getPenalties() {
        return Penalties;
    }

    public void setPenalties(String penalties) {
        Penalties = penalties;
    }

    public String getFree_kick_accuracy() {
        return Free_kick_accuracy;
    }

    public void setFree_kick_accuracy(String free_kick_accuracy) {
        Free_kick_accuracy = free_kick_accuracy;
    }

}
