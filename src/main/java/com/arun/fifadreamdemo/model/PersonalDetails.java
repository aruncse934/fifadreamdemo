package com.arun.fifadreamdemo.model;

public class PersonalDetails {

    private String Name;
    private int Age;
    private String Photo;
    private String Value;
    private String Wage;


    public PersonalDetails(String name, int age, String photo, String value, String wage) {
        Name = name;
        Age = age;
        Photo = photo;
        Value = value;
        Wage = wage;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getWage() {
        return Wage;
    }

    public void setWage(String wage) {
        Wage = wage;
    }

}
